# -*- coding: utf-8 -*-
import ui
from console import hud_alert
from hsr import HSR


def pose_action(sender):
    poses = {u"初期姿勢": {'arm_flex_joint': -0.03,
                           'arm_lift_joint': 0.01,
                           'arm_roll_joint': 0.0,
                           'head_pan_joint': 0.0,
                           'head_tilt_joint': 0.0,
                           'wrist_flex_joint': -1.5700005679771634,
                           'wrist_roll_joint': 0.0},
             u"腕を上に": {'arm_flex_joint': -0.03,
                           'arm_lift_joint': 0.5,
                           'arm_roll_joint': 0.0,
                           'head_pan_joint': 0.0,
                           'head_tilt_joint': 0.0,
                           'wrist_flex_joint': -1.5700005679771634,
                           'wrist_roll_joint': 0.0}
    }
    if sender.title in poses:
        h.pose(poses[sender.title])
        hud_alert(sender.title)


def say_action(sender):
    h.say(sender.title)
    hud_alert(sender.title)


def switch_action(sender):
    pass


def move_action(sender):
    pass


h = HSR()
h.connect("http://172.29.20.208:11311", "172.29.20.49") #

v = ui.load_view()
v.present('sheet')
