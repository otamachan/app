# -*- coding: utf-8 -*-
import os
import rospy
import tmc_msgs.msg
import sensor_msgs.msg


class HSR(object):
    def __init__(self):
        self.talk_pub = None
        self.pose_pub = None

    def connect(self, master_uri, ros_ip=None):
        os.environ['ROS_MASTER_URI'] = master_uri
        if ros_ip is None:
            import socket
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            s.connect(('8.8.8.8', 53))
            ros_ip = s.getsockname()[0]
            s.close()
        os.environ['ROS_IP'] = ros_ip
        os.environ['ROS_PYTHON_LOG_CONFIG_FILE'] = ''
        rospy.init_node('iphone', anonymous=True)
        self.talk_pub = rospy.Publisher('/talk_request',
                                        tmc_msgs.msg.Voice, queue_size=1)
        self.pose_pub = rospy.Publisher('/safe_pose_changer/joint_reference',
                                        sensor_msgs.msg.JointState, queue_size=1)

    def say(self, text):
        if self.talk_pub:
            self.talk_pub.publish(sentence=text)

    def pose(self, pose):
        if self.pose_pub:
            js = sensor_msgs.msg.JointState()
            for joint, value in pose.iteritems():
                js.name.append(joint)
                js.position.append(value)
            self.pose_pub.publish(js)

    def move(self, controller, positions):
        print controller
